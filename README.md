# AdoptLife_Frontend

## Preparación del proyecto
Antes de comenzar a trabajar en el proyecto se deben instalar las dependencias que se requiren para su correcto funcionamiento, para ello en la terminal se debe ejecutar el comando:
```
npm install
```
o la versión corta del comando:
```
npm i
```

### Iniciar el servidor de desarrollo
Una vez realizadas las instalaciones ya se pude empezar el servidor de desarrollo, para ello se utiliza el comando:
```
npm run serve
```

### Compilación y preparación para producción
Cuando ya esté listo el proyecto, al ejecutar el siguiente comando se crea una versión preparada y lista para subir a un servidor:
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
